This is a reverse-engineered HTTP server.



Currently, it supports a few MIME types (image/{gif, jpeg, ico}, text/{html, plain}, and application/java-archive).
If you read the code, you will quickly figure out it is pretty easy to add another to the decision structure in
HTTPServer.sendFile(). Just make sure that the correct output stream is used for your data; text needs to be
written with OutputStreamWriter and data such as images and other byte streams require DataOutputStream
(or the equivalents).



There are a bunch of things hard coded into the top of HTTPServer as constants. They will not work on your
machine unless you were to create all of those directories precisely.


Here's a quick rundown of the currently defined constants:


PATH: where server files are stored.

*_HTML: .html file that corresponds to *

*_{100, 200, 400, 404, 501} = status code data (probably won't need to be touched unless you're working in

some HTTP I'm not familiar with)

METHODS: request methods (also probably won't need to be changed)



The server logs HTTP requests in separate files so that I can learn from them as I go along. You can remove this

functionality by commenting out or removing these lines in HTTPServer.processClientStream():


bfw.write(s);

bfw.newLine();

bfw.flush();


And then you can also remove the FileWriter and all the code attachments in Client.



Have fun, and let me know stuff!