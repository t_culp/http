package com.caiolis.HTTP;

import java.net.Socket;
import java.util.ArrayList;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Client {
	
	private static ArrayList<Client> clientList = new ArrayList<Client>();
	private Socket socket = null;
	private String fileName = null;
	private BufferedWriter fileWriter = null;
	private DataOutputStream dataWriter = null;
	private OutputStreamWriter charWriter = null;
	private BufferedReader reader = null;
	private String ipAddress = null;
	private File requestedFile = null;
	private boolean isOldProtocol = false;
	private String statusCode = null;
	private String method = null;
	private long logonTime;
	
	public Client(Socket socket) {
		this.socket = socket;
		try { // because of the race conditions, there is no assignment outside of the synchronized statement in assignFileName
			fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(assignFileNameAndToList(this))));
			dataWriter = new DataOutputStream(socket.getOutputStream());
			charWriter = new OutputStreamWriter(socket.getOutputStream());
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			// The following lines write information about the client to its file and the console.
			ipAddress = socket.getInetAddress().getHostAddress();
			logonTime = System.currentTimeMillis();
			fileWriter.write("IP Address: " + ipAddress);
			for (int i = 0; i < 2; ++i)
				fileWriter.newLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(ipAddress + " has connected on " + HTTPServer.getServerTime(true));
	} // close Client
	
	private static synchronized String assignFileNameAndToList(Client c) {
		clientList.add(c);
		String time = HTTPServer.getServerTime(true);
		String fileName = time + ".txt";
		boolean nameFound = false;
		int tries = 0;
		while (!nameFound) {
			nameFound = true;
			for (Client client : clientList) {
				if (!client.equals(c) && fileName.equals(client.fileName)) {
					fileName = time + "(" + ++tries + ").txt";
					nameFound = false;
					break;
				}
			}
		}
		return (c.fileName = fileName);	
	} // close assignFileName
	
	public String getFileName() {
		return fileName;
	} // close getFileName
	
	public BufferedWriter getFileWriter() {
		return fileWriter;
	} // close clientFileWriter
	
	public OutputStreamWriter getCharWriter() {
		return charWriter;
	} // close getClientCharWriter
	
	public DataOutputStream getDataWriter() {
		return dataWriter;
	} // close getClientDataWriter
	
	public BufferedReader getReader() {
		return reader;
	} // close getClientReader
	
	public String getIPAddress() {
		return ipAddress;
	} // close getIPAddress
	
	public void setRequestedFile(File file) {
		requestedFile = file;
	} // close setRequetsedFile
	
	public File getRequestedFile() {
		return requestedFile;
	} // close getRequestedFile
	
	public void setOldProtocol() {
		isOldProtocol = true;
	} // close setOldProtocol
	
	public boolean isOldProtocol() {
		return isOldProtocol;
	} // close isOldProtocol
	
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	} // close setStatusCode
	
	public String getStatusCode() {
		return statusCode;
	} // close getStatusCode

	public void setMethod(String method) {
		this.method = method;
	} // close setMethod

	public String getMethod() {
		return method;
	} // close getMethod
	
	public void remove() throws IOException {
		if(!socket.isClosed())
			socket.close();
		fileWriter.close();
		charWriter.close();
		dataWriter.close();
		reader.close();
		clientList.remove(this);
		System.out.println(ipAddress + " has disconnected on " + HTTPServer.getServerTime(true) + " (connected for " + getTimeConnected() + ")");
	} // close remove
	
	private String getTimeConnected() {
		int totalSeconds = (int) ((System.currentTimeMillis() - logonTime)/1000);
		int seconds = totalSeconds % 60;
		int totalMinutes = totalSeconds / 60;
		int minutes = totalMinutes % 60;
		int totalHours = totalMinutes / 60;
		int hours = totalHours % 60;
		int days = totalHours / 60;
		return (days + ((hours < 10) ? ":0" : ":") + hours + ((minutes < 10) ? ":0" : ":") + minutes + ((seconds < 10) ? ":0" : ":") + seconds);
	} // close getTimeConnected
	
} // close Client
