package com.caiolis.HTTP;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Calendar;
import java.util.TimeZone;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class HTTPServer {
	
	private static int HTTP = 2015;
	private ServerSocket serverSocket;
	private static final String[] METHODS = {"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS", "TRACE", "CONNECT"};
	private static final String[] SUPPORTED_METHODS = {"GET", "HEAD", "OPTIONS"};
	private static final String CONTINUE_100 = "HTTP/1.1 100 Continue\r\n\r\n";
	private static final String OK_200 = "200 OK\r\n";
	private static final String BAD_REQUEST_400 = "400 Bad Request\r\n";
	private static final String NOT_FOUND_404 = "404 Not Found\r\n";
	private static final String NOT_IMPLEMENTED_501 = "501 Not Implemented\r\n";
	private static final String PATH = "D:/Thomas/Dropbox/server/";
	private static final String BAD_REQUEST_HTML = "400.html";
	private static final String NOT_FOUND_HTML = "404.html";
	private static final String NOT_IMPLEMENTED_HTML = "501.html";
	private static final String ROOT_HTML = "root.html";
	
	public static void main(String[] args) {
		HTTPServer server = new HTTPServer();
		if (server.serverSocket == null)
			System.exit(0);
		System.out.println("All systems go, cap'n! Port " + HTTP + " is ready to be boarded on " + getServerTime(false));
		server.acceptClients();
	} // close main
	
	public HTTPServer() {
		try {
			serverSocket = new ServerSocket(HTTP);
		} catch (BindException e) { // Caught if anything else is using the port (most often another running server)
			System.out.println("Could not acquire socket because an application is using port " + HTTP);
		} catch (IOException e) { // The catch-all, probably won't happen and it's unknown what or why would cause this error. See stack trace.
			System.out.println("Could not acquire socket for unknown reasons.");
			e.printStackTrace();
		}
	} // close constructor
	
	
	public static String getServerTime(boolean fileName) { // The parameter formats the date string so that it is acceptable for a file name or for HTTP
		SimpleDateFormat dateFormat;
		if (fileName) {
			dateFormat = new SimpleDateFormat("EEE-dd-MMM-yyyy-HHmmss-z", Locale.US);
			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT-4"));
			return dateFormat.format(Calendar.getInstance().getTime()).replaceAll(":", "");
		} else {
			dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
			dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
			return dateFormat.format(Calendar.getInstance().getTime());
		}
	} // close getServerTime
	
	public void acceptClients() {
		try {
			while (true) { // This will execute the entire time the server is operational. Maybe it'd be a good idea to have a better shut down operation
				final Socket clientSocket = serverSocket.accept();
				(new Thread(new Runnable() {
					public void run() {
						processClientStream(new Client(clientSocket));
					}
				})).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	} // close acceptClients
	
	private void processClientStream(Client c) {
		BufferedReader br = c.getReader();
		BufferedWriter bfw = c.getFileWriter();
		try {
			String s;
			boolean hasHost = false;
			while ((s = br.readLine()) != null) {
				bfw.write(s);
				bfw.newLine();
				bfw.flush();
				if (c.getRequestedFile() == null) {
					firstLineHandler(c, s);
				} else if (s.split(" ", 2)[0].equals("Host:")) {
					hasHost = true;
				} else if (s.equals("")) {
					if (!hasHost) {
						c.setStatusCode(BAD_REQUEST_400);
						c.setRequestedFile(new File(PATH + BAD_REQUEST_HTML));
					}
					sendFile(c);
					c.setRequestedFile(null);
					hasHost = false;
				}
			}
		} catch (SocketException e) {
			System.out.println("Client closed socket.");
		} catch (IOException e) {
			e.printStackTrace(); 
		} finally {
		       	try {
				c.remove(); // closes everything that can be closed about the client 
			} catch (IOException e) {
				System.out.println("WTH! My stuff won't close");
			}
		}
	} // close processClientFile
	
	
	// Sends the file stored in the Client instance. Other information stored in the instance (status code, protocol type) is sent as well.
	private void sendFile(Client c) throws IOException {
		OutputStreamWriter osw = c.getCharWriter();
		String statusCode = c.getStatusCode();
		String headers = "";
		if (c.isOldProtocol()) {
			headers += "HTTP/1.0 " + statusCode;
		} else {
			if (statusCode.equals(OK_200)) {
				osw.write(CONTINUE_100);
				osw.flush();
			}
			headers += "HTTP/1.1 " + statusCode;
		}
		headers += "Date: " + getServerTime(false) + "\r\n"; // add time 
		if (statusCode.equals(NOT_IMPLEMENTED_501) || c.getMethod().equals(SUPPORTED_METHODS[2])) { // Determines the need for an Allow line
			headers += "Allow: ";
			int i;
			for (i = 0; i < SUPPORTED_METHODS.length - 1; ++i)
				headers += (SUPPORTED_METHODS[i] + ", ");
			headers += (SUPPORTED_METHODS[i] + "\r\n");
			if (c.getMethod().equals(SUPPORTED_METHODS[2])) {
				headers += "Content-Length: 0\r\n\r\n";
				osw.write(headers);
				osw.flush();
				System.out.println(c.getIPAddress() + ": requested OPTIONS");
				return;
			}
		}
		headers += "Content-Type: "; // determine MIME type
		File file = c.getRequestedFile();
		String fileName = file.getName();
		String fileExtension = fileName.replace(PATH, "").split("\\.")[1];
		if (fileExtension.equalsIgnoreCase("gif"))
			headers += "image/gif";
		else if (fileExtension.equalsIgnoreCase("ico"))
			headers += "image/ico";
		else if (fileExtension.equalsIgnoreCase("jpg"))
			headers += "image/jpeg";
		else if (fileExtension.equalsIgnoreCase("html"))
			headers += "text/html";
		else if (fileExtension.equalsIgnoreCase("jar"))
			headers += "application/java-archive";
		else if (fileExtension.equalsIgnoreCase("mp3"))
			headers += "audio/mpeg";
		else
			headers += "text/plain";
		int fileSize = (int) file.length();
		headers += ("\r\nContent-Length: " + fileSize + "\r\n\r\n"); // determine size
		osw.write(headers); // begin writing
		if (c.getMethod().equals(METHODS[1])) {
			osw.flush();
			System.out.println(c.getIPAddress() + ": HEAD of " + fileName);
			return;
		} 
		FileInputStream fis = new FileInputStream(file); 
		if (headers.contains("text")) { // This decision determines if a char writer should be used instead of a data writer
			int i;
			while ((i = fis.read()) != -1)
				osw.write(i);
			osw.flush();
			fis.close();
		} else {
			osw.flush();
			byte[] binary = new byte[fileSize];
			fis.read(binary);
			c.getDataWriter().write(binary);
			c.getDataWriter().flush();
		}
		System.out.println(c.getIPAddress() + ": " + fileName);
		fis.close();
	} // close sendRequestedFile
	
	/*
	 * Handles first line of client stream and determines the requested file, an appropriate status code, and the protocol being used.
	 * This data is then stored in the Client instance.
	 */
	private void firstLineHandler(Client c, String s) throws IOException {
		String[] firstLine = s.split(" ");
		String firstWord = firstLine[0];
		boolean isFirstLine = false;
		for (String method : METHODS)
			if (firstWord.equals(method)) {
				isFirstLine = true;
				break;
			}
		if (!isFirstLine)
			return;
		c.setMethod(firstWord);
		File requestedFile = null;
		String statusCode = OK_200;
		boolean supported = false;
		for (String method : SUPPORTED_METHODS)
			if (firstWord.equals(method)) {
				supported = true;
				break;
			}
		if (supported) {
			String fileName = firstLine[1].substring(1, firstLine[1].length());
			if (fileName.equals("")) {
				requestedFile = new File(PATH + ROOT_HTML);
			} else if (!(requestedFile = new File(PATH + fileName)).isFile()) {
				requestedFile = new File(PATH + NOT_FOUND_HTML);
				statusCode = NOT_FOUND_404;
			}
		} else {
			System.out.println("I don't know how to handle " + firstWord + ", continuing as normal.");
			requestedFile = new File(PATH + NOT_IMPLEMENTED_HTML);
			statusCode = NOT_IMPLEMENTED_501;
		}
		c.setRequestedFile(requestedFile);
		c.setStatusCode(statusCode);
		if (firstLine[2].equals("HTTP/1.0"))
			c.setOldProtocol();
		if (firstLine.length > 3)
			System.out.println("Reached a first line longer than 3 elements long, continuing as normal");
	} //close firstLineHandler
	
} // close HTTPServer
